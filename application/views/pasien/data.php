<?php $this->load->view('template/header'); ?>

<div class="container-fluid">

    <!-- Page Heading -->

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h5>Data Pasien</h5>
            <!-- <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6> -->
            <button type="button" class="btn btn-info btn-sm" onclick="tambah_pasien()"><i class="fa fa-plus"></i> Tambah Pasien</button>
        </div>
        <div class="card-body">
            <div style="margin-bottom: 10px;">
                Keterangan : <a class="btn btn-success btn-sm"><i class="fa fa-edit"></i> </a> : Edit |
                <a class="btn btn-danger btn-sm "><i class="fa fa-trash"></i> </a> : Hapus |
                <a class="btn btn-warning btn-sm "><i class="fa fa-chart-line"></i> </a> : Monitoring Pengukuran |
                <a class="btn btn-info btn-sm "><i class="fa fa-chart-area"></i> </a> : Grafik IMT
            </div>
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>No.RM</th>
                            <th>Nama</th>
                            <th>Jenis Kelamin</th>
                            <th>Usia</th>
                            <th>Domisili</th>
                            <th>Tindakan</th>
                        </tr>
                    </thead>
            </div>
        </div>
    </div>
</div>
<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5> Edit data pasien</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <!-- <h3 class="modal-title">Person Form</h3> -->
            </div>
            <div class="modal-body form">
                <form id="form-data">
                    <input type="hidden" name="id">
                    <!-- <div class="col-md-6"> -->
                    <div class="form-group row">
                        <label for="text" class="col-4 col-form-label">Nama</label>
                        <div class="col-8">
                            <input id="nama" name="nama" placeholder="Nama Pasien" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="norm" class="col-4 col-form-label">No. RM</label>
                        <div class="col-8">
                            <input id="norm" name="norm" placeholder="No. Rm" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-4">Jeni kelamin</label>
                        <div class="col-8">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input name="jenis_kelamin" id="jenis_kelamin_0" type="radio" class="custom-control-input" value="L">
                                <label for="jenis_kelamin_0" class="custom-control-label">Laki-Laki</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input name="jenis_kelamin" id="jenis_kelamin_1" type="radio" class="custom-control-input" value="P">
                                <label for="jenis_kelamin_1" class="custom-control-label">Perempuan</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="text1" class="col-4 col-form-label">Tanggal Lahir</label>
                        <div class="col-8">
                            <div class="input-group">
                                <input id="tgl_lahir" name="tgl_lahir" type="text" class="form-control datepicker">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="domisili" class="col-4 col-form-label">Domisili</label>
                        <div class="col-8">
                            <select name="domisili" id="domisili" class="form-control">
                                <option value="Kodya Malang">Kodya Malang</option>
                                <option value="Kabupaten Malang">Kabupaten Malang</option>
                                <option value="Batu">Batu</option>
                                <option value="Pasuruan">Pasuruan</option>
                                <option value="Probolinggo">Probolinggo</option>
                                <option value="Lumajang">Lumajang</option>
                                <option value="Blitar">Blitar</option>
                                <option value="Lain-lain">Lain-lain</option>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button name="submit" type="button" class="btn btn-warning" onclick="batal()">Batal</button>
                <button name="submit" type="button" class="btn btn-primary" onclick="simpan()">Simpan</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
<?php $this->load->view('template/footer'); ?>
<script type="text/javascript">
    function tambah_pasien() {
        window.location.href = '<?= base_url('/pasien/tambah'); ?>';
    }
    var table;

    $(document).ready(function() {
        //datatables
        table = $('#dataTable').DataTable({
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": '<?php echo site_url('pasien/ajax_list'); ?>',
                "type": "POST"
            },

        });

    });

    function hapus(id) {
        swal({
                title: "Apakah anda yakin?",
                text: "Data tidak dapat dikembalikan!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((simpan) => {
                if (simpan) {
                    $.ajax({
                        type: 'POST',
                        url: "<?php echo base_url(); ?>/pasien/hapus",
                        data: {
                            id: id
                        },
                        cache: false,
                        dataType: 'json',
                        success: function(data) {
                            console.log(data);
                            if (data.code == 200) {
                                swal({
                                    title: "Sukses",
                                    text: data.msg,
                                    icon: "success",
                                    button: "Ok",
                                }).then(function() {
                                    table.ajax.reload();
                                })
                            } else {
                                swal({
                                    title: "Gagal",
                                    text: data.msg,
                                    icon: "error",
                                    button: "Ok",
                                });
                            }
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            swal({
                                title: "Gagal",
                                text: xhr.status,
                                icon: "error",
                                button: "Ok",
                            });
                        }
                    });
                }
            });
    }

    function edit(id) {
        $('#form-data')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
            url: "<?php echo site_url('pasien/ajax_edit/') ?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $('[name="id"]').val(data.id);
                $('[name="nama"]').val(data.nama);
                $('[name="norm"]').val(data.norm);
                $('[name="domisili"]').val(data.domisili);
                if (data.jenis_kelamin == 'L') {
                    $("#jenis_kelamin_0").prop("checked", true);
                } else {
                    $("#jenis_kelamin_1").prop("checked", true);
                }
                $('[name="tgl_lahir"]').datepicker('update', data.tgl_lahir);
                $('#modal_form').modal('show');

            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error get data from ajax');
            }
        });
    }

    function simpan() {
        // alert($('input[name="jenis_kelamin"]:checked').val())
        if ($('#nama').val() == '') {
            alert("Nama Harus Diisi");
            $('#nama').focus();
            return false;
        }
        if ($('#norm').val() == '') {
            alert("No.RM Harus Diisi");
            $('#nama').focus();
            return false;
        }
        if ($('input[name="jenis_kelamin"]:checked').val() == undefined) {
            alert('Jenis kelamin belum diisi');
            return false;
        }
        if ($('#tgl_lahir').val() == '') {
            alert("Tanggal Lahir Harus Diisi");
            $('#tgl_lahir').focus();
            return false;
        }
        if ($('#domisili').val() == '') {
            alert("DomisiliHarus Diisi");
            $('#domisili').focus();
            return false;
        }
        data = $('#form-data').serialize();
        swal({
                title: "Apakah anda yakin?",
                text: "Pastikan data yang diinputkan benar!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((simpan) => {
                if (simpan) {
                    $.ajax({
                        type: 'POST',
                        url: "<?php echo base_url(); ?>/pasien/simpan_edit",
                        data: data,
                        cache: false,
                        dataType: 'json',
                        success: function(data) {
                            console.log(data);
                            if (data.code == 200) {
                                swal({
                                    title: "Sukses",
                                    text: data.msg,
                                    icon: "success",
                                    button: "Ok",
                                }).then(function() {
                                    // location.reload();
                                    $('#modal_form').modal('hide');
                                    table.ajax.reload();

                                })
                            } else {
                                swal({
                                    title: "Gagal",
                                    text: data.msg,
                                    icon: "error",
                                    button: "Ok",
                                });
                            }
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            swal({
                                title: "Gagal",
                                text: xhr.status,
                                icon: "error",
                                button: "Ok",
                            });
                        }
                    });
                }
            });

    }

    function batal() {
        $('#modal_form').modal('hide');
    }

    function monitoring(id) {
        window.location.href = '<?= base_url('/pengukuran/data/'); ?>' + id;
    }

    function grafik(id) {
        window.location.href = '<?= base_url('/pengukuran/imt/'); ?>' + id;
    }
</script>