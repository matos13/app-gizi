<?php $this->load->view('template/header'); ?>

<div class="container-fluid">

    <!-- Page Heading -->
    <!-- <h1 class="h3 mb-2 text-gray-800">Tambah Pasien</h1> -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h5>Form Tambah Data Pasien</h5>
        </div>
        <div class="card-body">
            <!-- <div class="row"> -->
            <form id="form-data">
                <!-- <div class="col-md-6"> -->
                <div class="form-group row">
                    <label for="text" class="col-4 col-form-label">Nama</label>
                    <div class="col-8">
                        <input id="nama" name="nama" placeholder="Nama Pasien" type="text" class="form-control">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="norm" class="col-4 col-form-label">No. RM</label>
                    <div class="col-8">
                        <input id="norm" name="norm" placeholder="No. Rm" type="text" class="form-control">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-4">Jeni kelamin</label>
                    <div class="col-8">
                        <div class="custom-control custom-radio custom-control-inline">
                            <input name="jenis_kelamin" id="jenis_kelamin_0" type="radio" class="custom-control-input" value="L">
                            <label for="jenis_kelamin_0" class="custom-control-label">Laki-Laki</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input name="jenis_kelamin" id="jenis_kelamin_1" type="radio" class="custom-control-input" value="P">
                            <label for="jenis_kelamin_1" class="custom-control-label">Perempuan</label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="text1" class="col-4 col-form-label">Tanggal Lahir</label>
                    <div class="col-8">
                        <div class="input-group">
                            <input id="tgl_lahir" name="tgl_lahir" type="text" class="form-control datepicker">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <i class="fa fa-calendar"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="domisili" class="col-4 col-form-label">Domisili</label>
                    <div class="col-8">
                        <!-- <textarea id="domisili" name="domisili" cols="40" rows="5" class="form-control"></textarea> -->
                        <select name="domisili" id="domisili" class="form-control">
                            <option value="Kodya Malang">Kodya Malang</option>
                            <option value="Kabupaten Malang">Kabupaten Malang</option>
                            <option value="Batu">Batu</option>
                            <option value="Pasuruan">Pasuruan</option>
                            <option value="Probolinggo">Probolinggo</option>
                            <option value="Lumajang">Lumajang</option>
                            <option value="Blitar">Blitar</option>
                            <option value="Lain-lain">Lain-lain</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-4 col-8">
                        <button name="submit" type="button" class="btn btn-warning" onclick="location.reload()">Batal</button>
                        <button name="submit" type="button" class="btn btn-primary" onclick="simpan()">Simpan</button>
                    </div>
                </div>
                <!-- </div> -->
            </form>
            <!-- </div> -->
        </div>
    </div>
</div>

<?php $this->load->view('template/footer'); ?>
<script type="text/javascript">
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });

    function simpan() {
        // alert($('input[name="jenis_kelamin"]:checked').val())
        if ($('#nama').val() == '') {
            alert("Nama Harus Diisi");
            $('#nama').focus();
            return false;
        }
        if ($('#norm').val() == '') {
            alert("No.RM Harus Diisi");
            $('#nama').focus();
            return false;
        }
        if ($('input[name="jenis_kelamin"]:checked').val() == undefined) {
            alert('Jenis kelamin belum diisi');
            return false;
        }
        if ($('#tgl_lahir').val() == '') {
            alert("Tanggal Lahir Harus Diisi");
            $('#tgl_lahir').focus();
            return false;
        }
        if ($('#domisili').val() == '') {
            alert("DomisiliHarus Diisi");
            $('#domisili').focus();
            return false;
        }
        data = $('#form-data').serialize();
        swal({
                title: "Apakah anda yakin?",
                text: "Pastikan data yang diinputkan benar!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((simpan) => {
                if (simpan) {
                    $.ajax({
                        type: 'POST',
                        url: "<?php echo base_url(); ?>/pasien/simpan",
                        data: data,
                        cache: false,
                        dataType: 'json',
                        success: function(data) {
                            console.log(data);
                            if (data.code == 200) {
                                swal({
                                    title: "Sukses",
                                    text: data.msg,
                                    icon: "success",
                                    button: "Ok",
                                }).then(function() {
                                    location.reload();
                                })
                            } else {
                                swal({
                                    title: "Gagal",
                                    text: data.msg,
                                    icon: "error",
                                    button: "Ok",
                                });
                            }
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            swal({
                                title: "Gagal",
                                text: xhr.status,
                                icon: "error",
                                button: "Ok",
                            });
                        }
                    });
                }
            });

    }
</script>