<?php $this->load->view('template/header'); ?>
<?php
$bulan = isset($_GET['bulan']) ? $_GET['bulan'] : date('m-Y');
?>
<div class="container-fluid">
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h5 >Laporan</h5>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <form action="" method="get">
                        <div class="form-group row">
                            <label for="text1" class="col-4 col-form-label">Periode Pemantauan</label>
                            <div class="col-8">
                                <div class="input-group">
                                    <input id="bulan" name="bulan" type="text" class="form-control bulan" value="<?php echo $bulan ?>">
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="offset-4 col-8">
                                <button type="button" class="btn btn-warning" onclick="location.reload()">Batal</button>
                                <button type="submit" class="btn btn-primary">Cari</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-12">
                    <?php
                    // print_r($laporan);
                    ?>
                    <div style="margin-bottom: 10px;">
                        <a class="btn btn-success btn-sm" onclick="excel()"><i class="fa fa-file-excel"></i> Download excel</a>
                    </div>
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <tr style="text-align:center ;">
                            <th rowspan="2" style="vertical-align : middle;">No</th>
                            <th rowspan="2" style="vertical-align : middle;">Tanggal Pengukuran</th>
                            <th rowspan="2" style="vertical-align : middle;">No.Rm</th>
                            <th rowspan="2" style="vertical-align : middle;">Nama</th>
                            <th rowspan="2" style="vertical-align : middle;">BB</th>
                            <th rowspan="2" style="vertical-align : middle;">TB</th>
                            <th rowspan="2" style="vertical-align : middle;">IMT</th>
                            <th colspan="5">Status Gizi</th>
                            <th colspan="3">Perubahan Berat Badan</th>
                            <th rowspan="2" style="vertical-align : middle;">Rencana Tindak Lanjut</th>
                        </tr>
                        <tr>
                            <th>Kurang BB Tingkat Berat</th>
                            <th>Kurang BB Tingkat Ringan</th>
                            <th>Normal</th>
                            <th>Lebih BB Tingkat Ringan</th>
                            <th>Lebih BB Tingkat Berat</th>
                            <th>Naik</th>
                            <th>Turun</th>
                            <th>Tetap</th>
                        </tr>
                        <?php
                        $kurang1 = 0;
                        $kurang2 = 0;
                        $lebih1 = 0;
                        $lebih2 = 0;
                        $normal = 0;
                        $turun = 0;
                        $naik = 0;
                        $tetap = 0;
                        foreach ($laporan as $key => $value) :
                            // print_r($value);
                            if ($value->status_gizi == 'Kurang BB Tingkat Berat') {
                                $kurang2++;
                            } elseif ($value->status_gizi == 'Kurang BB Tingkat Ringan') {
                                $kurang1++;
                            } elseif ($value->status_gizi == 'Normal') {
                                $normal++;
                            } elseif ($value->status_gizi == 'Lebih BB Tingkat Ringan') {
                                $lebih1++;
                            } elseif ($value->status_gizi == 'Lebih BB Tingkat Berat') {
                                $lebih2++;
                            }
                            if ($value->keterangan == 'Naik') {
                                $naik++;
                            } elseif ($value->keterangan == 'Turun') {
                                $turun++;
                            } elseif ($value->keterangan == 'Tetap') {
                                $tetap++;
                            }
                        ?>
                            <tr>
                                <td><?php echo $key + 1 ?></td>
                                <td><?php echo tgl_indo($value->tanggal)?></td>
                                <td><?php echo $value->norm ?></td>
                                <td><?php echo $value->nama ?></td>
                                <td><?php echo $value->bb?></td>
                                <td><?php echo $value->tb?></td>
                                <td><?php echo $value->imt?></td>
                                <td align="center"><?php echo ($value->status_gizi == 'Kurang BB Tingkat Berat' ? '&check;' : '') ?></td>
                                <td align="center"><?php echo ($value->status_gizi == 'Kurang BB Tingkat Ringan' ? '&check;' : '') ?></td>
                                <td align="center"><?php echo ($value->status_gizi == 'Normal' ? '&check;' : '') ?></td>
                                <td align="center"><?php echo ($value->status_gizi == 'Lebih BB Tingkat Ringan' ? '&check;' : '') ?></td>
                                <td align="center"><?php echo ($value->status_gizi == 'Lebih BB Tingkat Berat' ? '&check;' : '') ?></td>
                                <td align="center"><?php echo ($value->keterangan == 'Naik' ? '&check;' : '') ?></td>
                                <td align="center"><?php echo ($value->keterangan == 'Turun' ? '&check;' : '') ?></td>
                                <td align="center"><?php echo ($value->keterangan == 'Tetap' ? '&check;' : '') ?></td>
                                <td><?php echo $value->tindak_lanjut?></td>
                            </tr>
                        <?php endforeach ?>
                        <tr>
                            <td colspan="7" align="right">Jumlah</td>
                            <td align="center"><?php echo $kurang2 ?></td>
                            <td align="center"><?php echo $kurang1 ?></td>
                            <td align="center"><?php echo $normal ?></td>
                            <td align="center"><?php echo $lebih1 ?></td>
                            <td align="center"><?php echo $lebih2 ?></td>
                            <td align="center"><?php echo $naik ?></td>
                            <td align="center"><?php echo $turun ?></td>
                            <td align="center"><?php echo $tetap ?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('template/footer'); ?>

<script>
    $('.bulan').datepicker({
        format: "mm-yyyy",
        viewMode: "months",
        minViewMode: "months",
        autoclose: true
    });

    function excel() {
        window.location.href = '<?= base_url('/laporan/export?bulan='); ?><?= $bulan ?>';
    }
</script>