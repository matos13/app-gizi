<?php $this->load->view('template/header'); ?>

<div class="container-fluid">

    <!-- Page Heading -->
    <?php
    $lahir        = new DateTime($pasien->tgl_lahir);
    $today        = new DateTime();
    $umur          = $today->diff($lahir);
    $umur         = $umur->y . " Tahun " . $umur->m . " Bulan " . $umur->d . " Hari";

    ?>
    <h1 class="h3 mb-2 text-gray-800">Monitoring Pengukuran</h1>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <button class="btn btn-sm btn-info" type="button" onclick="kembali()"> <i class="fa fa-arrow-circle-left"></i> Kembali</button>
        </div>
        <div class="card-body">
            <table style="width: 50%;">
                <tr>
                    <td style="width: 20%;">Nama Pasien</td>
                    <td>:</td>
                    <td><?php echo $pasien->nama ?></td>
                </tr>
                <tr>
                    <td>No.Rm</td>
                    <td>:</td>
                    <td><?php echo $pasien->norm ?></td>
                </tr>
                <tr>
                    <td>Jenis Kelamin</td>
                    <td>:</td>
                    <td><?php echo ($pasien->jenis_kelamin == 'L' ? 'Laki-laki' : 'Perempuan') ?></td>
                </tr>
                <tr>
                    <td>Usia</td>
                    <td>:</td>
                    <td><?php echo $umur ?></td>
                </tr>
                <tr>
                    <td>Domisili</td>
                    <td>:</td>
                    <td><?php echo $pasien->domisili ?></td>
                </tr>
            </table>
            <br>
            <button class="btn btn-sm btn-success" type="button" onclick="tambah()"> <i class="fa fa-plus"></i> Tambah Pengukuran</button>
            <div class="table-responsive" style="margin-top:5px">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th style="vertical-align: middle;text-align: center;" rowspan="2">No</th>
                            <th style="vertical-align: middle;text-align: center;" rowspan="2">Tanggal Ukur</th>
                            <th style="vertical-align: middle;text-align: center;" rowspan="2">BB(Kg)</th>
                            <th style="vertical-align: middle;text-align: center;" rowspan="2">TB(Cm)</th>
                            <th style="vertical-align: middle;text-align: center;" rowspan="2">BBI(Kg)</th>
                            <th style="vertical-align: middle;text-align: center;" rowspan="2">IMT(Kg/ms<sup>2</sup>)</th>
                            <th style="vertical-align: middle;text-align: center;" rowspan="2">Status Gizi</th>
                            <th style="vertical-align: middle;text-align: center;" colspan="2">Perubahan</th>
                            <th style="vertical-align: middle;text-align: center;" rowspan="2" style="width: 6%;">Keterangan</th>
                            <th style="vertical-align: middle;text-align: center;" rowspan="2" style="width: 6%;">Rencana Tindak Lanjut</th>
                            <th style="vertical-align: middle;text-align: center;" rowspan="2">Tindakan</th>
                        </tr>
                        <tr>
                            <th style="vertical-align: middle;text-align: center;">Kg</th>
                            <th style="vertical-align: middle;text-align: center;">%</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        // print_r($pengukuran);
                        // exit;
                        foreach ($pengukuran as $key => $value) : ?>
                            <tr>
                                <td align="center"><?php echo $key + 1 ?></td>
                                <td align="center"><?php echo tgl_indo($value->tanggal) ?></td>
                                <td align="center"><?php echo $value->bb ?></td>
                                <td align="center"><?php echo $value->tb ?></td>
                                <td align="center"><?php echo $value->bbi ?></td>
                                <td align="center"><?php echo $value->imt ?></td>
                                <td align="center"><?php echo $value->status_gizi ?></td>
                                <td align="center"><?php echo ($value->perubahan_kg == 0 ? '' : $value->perubahan_kg) ?></td>
                                <td align="center"><?php echo ($value->perubahan_persen == 0 ? '' : $value->perubahan_persen) ?></td>
                                <td align="center"><?php echo $value->keterangan ?></td>
                                <td align="center"><?php echo $value->tindak_lanjut ?></td>
                                <td align="center"><a class="btn btn-success btn-sm" onclick="edit(<?php echo $value->id ?>)"><i class="fa fa-edit"></i> </a>
                                    <a class="btn btn-danger btn-sm" onclick="hapus(<?php echo $value->id ?>)"><i class="fa fa-trash"></i> </a>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
                <div style="margin-bottom: 10px;">
                    Keterangan : <a class="btn btn-success btn-sm"><i class="fa fa-edit"></i> </a> : Edit |
                    <a class="btn btn-danger btn-sm "><i class="fa fa-trash"></i> </a> : Hapus
                </div>
            </div>
        </div>
    </div>
    <!-- Bootstrap modal -->
    <div class="modal fade" id="modal_form" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5>Pengukuran</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <!-- <h3 class="modal-title">Person Form</h3> -->
                </div>
                <div class="modal-body form">
                    <form id="form-data">
                        <input type="hidden" name="id">
                        <input type="hidden" name="jk" id="jk" value="<?php echo $pasien->jenis_kelamin ?>">
                        <input type="hidden" name="id_pasien" id="id_pasien" value="<?php echo $pasien->id ?>">
                        <input type="hidden" name="bb_awal" id="bb_awal" value="<?php echo !empty($bbawal) ? $bbawal[0]->bb : '' ?>">
                        <input type="hidden" name="id_awal" id="id_awal" value="<?php echo !empty($bbawal) ? $bbawal[0]->id : '' ?>">
                        <!-- <div class="col-md-6"> -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label for="text1" class="col-4 col-form-label">Tanggal Pengukuran</label>
                                    <div class="col-8">
                                        <div class="input-group">
                                            <input id="tgl_ukur" name="tgl_ukur" type="text" class="form-control datepicker">
                                            <div class="input-group-append">
                                                <div class="input-group-text">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="bb" class="col-4 col-form-label">Berat badan</label>
                                    <div class="col-8">
                                        <div class="input-group">
                                            <input id="bb" name="bb" placeholder="Berat badan" type="text" class="form-control" onkeyup="Desimal(this);Hitung()">
                                            <div class="input-group-append">
                                                <div class="input-group-text">Kg</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="tb" class="col-4 col-form-label">Tinggi Badan</label>
                                    <div class="col-8">
                                        <div class="input-group">
                                            <input id="tb" name="tb" placeholder="Tinggi Badan" type="text" class="form-control" onkeyup="Desimal(this);Hitung()">
                                            <div class="input-group-append">
                                                <div class="input-group-text">Cm</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="tb" class="col-4 col-form-label">BBI</label>
                                    <div class="col-8">
                                        <div class="input-group">
                                            <input id="bbi" name="bbi" type="text" class="form-control" readonly>
                                            <div class="input-group-append">
                                                <div class="input-group-text">Kg</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="tb" class="col-4 col-form-label">IMT</label>
                                    <div class="col-8">
                                        <div class="input-group">
                                            <input id="imt" name="imt" type="text" class="form-control" readonly>
                                            <div class="input-group-append">
                                                <div class="input-group-text">Kg/m<sup>2</sup></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="text" class="col-4 col-form-label">Status Gizi </label>
                                    <div class="col-8">
                                        <input id="status" name="status" type="text" class="form-control" readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="text" class="col-4 col-form-label">Keterangan</label>
                                    <div class="col-8">
                                        <input id="keterangan" name="keterangan" type="text" class="form-control" readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="tb" class="col-4 col-form-label">Perubahan BB</label>
                                    <div class="col-4">
                                        <div class="input-group">
                                            <input id="bb_kg" name="bb_kg" type="text" class="form-control" readonly>
                                            <div class="input-group-append">
                                                <div class="input-group-text">Kg</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="input-group">
                                            <input id="bb_persen" name="bb_persen" type="text" class="form-control" readonly>
                                            <div class="input-group-append">
                                                <div class="input-group-text">%</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="text" class="col-4 col-form-label">Rencana Tindak Lanjut</label>
                                    <div class="col-8">
                                        <textarea name="tindak_lanjut" id="tindak_lanjut" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button name="submit" type="button" class="btn btn-warning" onclick="batal()">Batal</button>
                    <button name="submit" type="button" class="btn btn-primary" onclick="simpan()">Simpan</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- End Bootstrap modal -->
    <?php $this->load->view('template/footer'); ?>
    <script type="text/javascript">
        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true
        });
        $(document).ready(function() {
            //datatables
            table = $('#dataTable').DataTable({
                "bPaginate": false,
                "bFilter": false,
                "bInfo": false
            });

        });

        function Desimal(obj) {
            a = obj.value;
            var reg = new RegExp(/[0-9]+(?:\.[0-9]{0,2})?/g)
            b = a.match(reg, '');
            if (b == null) {
                obj.value = '';
            } else {
                obj.value = b[0];
            }

        }

        function Hitung() {
            var bbi;
            var bb;
            var tb;
            var gizi;
            jk = $('#jk').val();
            bb_awal = $('#bb_awal').val();

            if ($('#bb').val() == '') {
                bb = 0;
            } else {
                bb = parseFloat($('#bb').val())
            }
            if ($('#tb').val() == '') {
                tb = 0;
            } else {
                tb = parseFloat($('#tb').val())
            }

            tb_kuadrat = (tb / 100) * (tb / 100)

            if (jk == 'L') {
                bbi = 22.5 * tb_kuadrat;
            } else {
                bbi = 21 * tb_kuadrat;
            }

            imt = bb / tb_kuadrat;

            if (imt < 17) {
                gizi = 'Kurang BB Tingkat Berat';
            } else if (imt >= 17 && imt < 18.5) {
                gizi = 'Kurang BB Tingkat Ringan';
            } else if (imt >= 18.5 && imt < 25.1) {
                gizi = 'Normal';
            } else if (imt >= 25.1 && imt <= 27) {
                gizi = 'Lebih BB Tingkat Ringan';
            } else {
                gizi = 'Lebih BB Tingkat Berat';

            }
            $('#bbi').val(bbi.toFixed(2));
            $('#imt').val(imt.toFixed(2));
            $('#status').val(gizi);

            if (bb_awal != '' && bb_awal != 0) {
                kg = parseFloat(bb_awal) - bb;
                if (parseFloat(bb_awal) == bb) {
                    ket = 'Tetap';
                } else if (parseFloat(bb_awal) > bb) {
                    ket = 'Turun';

                } else {

                    ket = 'Naik';
                }

                persen = ((parseFloat(bb_awal) - bb) / parseFloat(bb_awal)) * 100;

                $('#keterangan').val(ket);
                $('#bb_persen').val(Math.abs(persen).toFixed(2));
                $('#bb_kg').val(Math.abs(kg.toFixed(2)));

            }
        }

        function kembali() {
            window.location.href = '<?= base_url('/pasien'); ?>';
        }

        function batal() {
            $('#modal_form').modal('hide');
        }

        function tambah() {
            $('#form-data')[0].reset(); // reset form on modals
            $('.form-group').removeClass('has-error'); // clear error class
            $('.help-block').empty(); // clear error string
            $('#modal_form').modal('show');

        }

        function edit(id) {
            $('#form-data')[0].reset(); // reset form on modals
            $('.form-group').removeClass('has-error'); // clear error class
            $('.help-block').empty(); // clear error string

            //Ajax Load data from ajax
            $.ajax({
                url: "<?php echo site_url('pengukuran/ajax_edit/') ?>/" + id,
                type: "GET",
                dataType: "JSON",
                success: function(data) {
                    console.log(data);
                    $('[name="id"]').val(data.id);
                    $('[name="bb"]').val(data.bb);
                    $('[name="tb"]').val(data.tb);
                    $('[name="bbi"]').val(data.bbi);
                    $('[name="imt"]').val(data.imt);
                    $('[name="tgl_ukur"]').val(data.tanggal);
                    $('[name="keterangan"]').val(data.keterangan);
                    $('[name="status"]').val(data.status_gizi);
                    $('[name="bb_kg"]').val(data.perubahan_kg);
                    $('[name="bb_persen"]').val(data.perubahan_persen);
                    $('[name="bb_awal"]').val(data.bb_awal);
                    $('[name="id_awal"]').val(data.id_pengukuran_sebelum);
                    $('[name="tindak_lanjut"]').val(data.tindak_lanjut);
                    $('#modal_form').modal('show');

                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Error get data from ajax');
                }
            });
        }

        function hapus(id) {
            swal({
                    title: "Apakah anda yakin?",
                    text: "Data tidak dapat dikembalikan!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((simpan) => {
                    if (simpan) {
                        $.ajax({
                            type: 'POST',
                            url: "<?php echo base_url(); ?>/pengukuran/hapus/" + id,
                            cache: false,
                            dataType: 'json',
                            success: function(data) {
                                // console.log(data);
                                if (data.code == 200) {
                                    swal({
                                        title: "Sukses",
                                        text: data.msg,
                                        icon: "success",
                                        button: "Ok",
                                    }).then(function() {
                                        location.reload();

                                    })
                                } else {
                                    swal({
                                        title: "Gagal",
                                        text: data.msg,
                                        icon: "error",
                                        button: "Ok",
                                    });
                                }
                            },
                            error: function(xhr, ajaxOptions, thrownError) {
                                swal({
                                    title: "Gagal",
                                    text: xhr.status,
                                    icon: "error",
                                    button: "Ok",
                                });
                            }
                        });
                    }
                });
        }

        function simpan() {
            // alert($('input[name="jenis_kelamin"]:checked').val())
            if ($('#tgl_ukur').val() == '') {
                alert("Tanggal Pengukuran Harus Diisi");
                $('#tgl_ukur').focus();
                return false;
            }
            if ($('#bb').val() == '') {
                alert("Berat Badan Harus Diisi");
                $('#bb').focus();
                return false;
            }
            if ($('#tb').val() == '') {
                alert("Tinggi Badan Harus Diisi");
                $('#tb').focus();
                return false;
            }
            data = $('#form-data').serialize();
            swal({
                    title: "Apakah anda yakin?",
                    text: "Pastikan data yang diinputkan benar!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((simpan) => {
                    if (simpan) {
                        $.ajax({
                            type: 'POST',
                            url: "<?php echo base_url(); ?>/pengukuran/simpan",
                            data: data,
                            cache: false,
                            dataType: 'json',
                            success: function(data) {
                                console.log(data);
                                if (data.code == 200) {
                                    swal({
                                        title: "Sukses",
                                        text: data.msg,
                                        icon: "success",
                                        button: "Ok",
                                    }).then(function() {
                                        location.reload();
                                    })
                                } else {
                                    swal({
                                        title: "Gagal",
                                        text: data.msg,
                                        icon: "error",
                                        button: "Ok",
                                    });
                                }
                            },
                            error: function(xhr, ajaxOptions, thrownError) {
                                swal({
                                    title: "Gagal",
                                    text: xhr.status,
                                    icon: "error",
                                    button: "Ok",
                                });
                            }
                        });
                    }
                });

        }
    </script>