<?php $this->load->view('template/header'); ?>

<div class="container-fluid">

    <!-- Page Heading -->
    <?php
    $lahir        = new DateTime($pasien->tgl_lahir);
    $today        = new DateTime();
    $umur          = $today->diff($lahir);
    $umur         = $umur->y . " Tahun " . $umur->m . " Bulan " . $umur->d . " Hari";

    ?>
    <style>
        #chartdiv {
            width: 100%;
            height: 500px;
        }
    </style>
    <h1 class="h3 mb-2 text-gray-800">Grafik IMT</h1>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <button class="btn btn-sm btn-info" type="button" onclick="kembali()"> <i class="fa fa-arrow-circle-left"></i> Kembali</button>
        </div>
        <div class="card-body">
            <table style="width: 50%;">
                <tr>
                    <td style="width: 20%;">Nama Pasien</td>
                    <td>:</td>
                    <td><?php echo $pasien->nama ?></td>
                </tr>
                <tr>
                    <td>No.Rm</td>
                    <td>:</td>
                    <td><?php echo $pasien->norm ?></td>
                </tr>
                <tr>
                    <td>Jenis Kelamin</td>
                    <td>:</td>
                    <td><?php echo ($pasien->jenis_kelamin == 'L' ? 'Laki-laki' : 'Perempuan') ?></td>
                </tr>
                <tr>
                    <td>Usia</td>
                    <td>:</td>
                    <td><?php echo $umur ?></td>
                </tr>
                <tr>
                    <td>Domisili</td>
                    <td>:</td>
                    <td><?php echo $pasien->domisili ?></td>
                </tr>
            </table>
            <br>
            <div class="table-responsive" style="margin-top:5px">
                <div class="ct-chart" style="height: 400px;">

                </div>
            </div>
        </div>
    </div>
    <?php
    $bulan = array();
    $isi = array();
    foreach ($pengukuran as $key => $value) {

        $bl = explode('-', $value->tanggal);
        array_push($bulan, bulan($bl[1]));
        array_push($isi, $value->imt);
    }
    // print_r(json_encode($bulan));

    ?>
    <?php $this->load->view('template/footer'); ?>

    <script>
        function kembali() {
            window.location.href = '<?= base_url('/pasien'); ?>';
        }
        new Chartist.Line('.ct-chart', {
            labels: <?php echo (json_encode($bulan)) ?>,
            series: [
                <?php echo (json_encode($isi)) ?>,
            ]
        }, {
            fullWidth: true,
            chartPadding: {
                right: 100
            },
            showArea: true
        });
    </script>