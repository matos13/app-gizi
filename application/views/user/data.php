<?php $this->load->view('template/header'); ?>

<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Daftar Pasien</h1>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <!-- <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6> -->
            <button type="button" class="btn btn-info btn-sm" onclick="tambah()"><i class="fa fa-plus"></i> Tambah User</button>
        </div>
        <div class="card-body">
            <div style="margin-bottom: 10px;">
                <!-- Keterangan : <a class="btn btn-success btn-sm"><i class="fa fa-edit"></i> </a> : Edit |
                <a class="btn btn-danger btn-sm "><i class="fa fa-trash"></i> </a> : Hapus -->
            </div>
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <!-- <th>Role</th> -->
                            <th>Aksi</th>
                        </tr>
                    </thead>
                </table>
                Keterangan : <a class="btn btn-success btn-sm"><i class="fa fa-edit"></i> </a> : Edit |
                <a class="btn btn-danger btn-sm "><i class="fa fa-trash"></i> </a> : Hapus |
                <a class="btn btn-warning btn-sm "><i class="fa fa-undo"></i> </a> : Reset Password
            </div>
        </div>
    </div>
</div>
<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5>User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body form">
                <form id="form-data">
                    <input type="hidden" name="id" id="id">
                    <!-- <div class="col-md-6"> -->
                    <div class="form-group row">
                        <label for="text" class="col-4 col-form-label">Nama</label>
                        <div class="col-8">
                            <input id="nama" name="nama" placeholder="Nama" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="email" class="col-4 col-form-label">Email</label>
                        <div class="col-8">
                            <input id="email" name="email" placeholder="Email" type="email" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row" id="div_password">
                        <label for="password" class="col-4 col-form-label">Password</label>
                        <div class="col-8">
                            <input id="password" name="password" placeholder="" type="password" class="form-control">
                            <span id="alert" style="display: none;"><i>Kosongkan jika tidak akan merubah password</i></span>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button name="submit" type="button" class="btn btn-warning" onclick="batal()">Batal</button>
                <button name="submit" type="button" class="btn btn-primary" onclick="simpan()">Simpan</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
<?php $this->load->view('template/footer'); ?>
<script type="text/javascript">
    var table;

    $(document).ready(function() {
        table = $('#dataTable').DataTable({
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.
            "ajax": {
                "url": '<?php echo site_url('pengguna/ajax_list'); ?>',
                "type": "POST"
            },

        });

    });

    function hapus(id) {
        swal({
                title: "Apakah anda yakin?",
                text: "Data tidak dapat dikembalikan!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((simpan) => {
                if (simpan) {
                    $.ajax({
                        type: 'POST',
                        url: "<?php echo base_url(); ?>/pengguna/hapus",
                        data: {
                            id: id
                        },
                        cache: false,
                        dataType: 'json',
                        success: function(data) {
                            console.log(data);
                            if (data.code == 200) {
                                swal({
                                    title: "Sukses",
                                    text: data.msg,
                                    icon: "success",
                                    button: "Ok",
                                }).then(function() {
                                    table.ajax.reload();
                                })
                            } else {
                                swal({
                                    title: "Gagal",
                                    text: data.msg,
                                    icon: "error",
                                    button: "Ok",
                                });
                            }
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            swal({
                                title: "Gagal",
                                text: xhr.status,
                                icon: "error",
                                button: "Ok",
                            });
                        }
                    });
                }
            });
    }
    function reset(id) {
        swal({
                title: "Apakah anda yakin?",
                text: "Pasword akan di reset menjadi 123",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((simpan) => {
                if (simpan) {
                    $.ajax({
                        type: 'POST',
                        url: "<?php echo base_url(); ?>/pengguna/reset",
                        data: {
                            id: id
                        },
                        cache: false,
                        dataType: 'json',
                        success: function(data) {
                            console.log(data);
                            if (data.code == 200) {
                                swal({
                                    title: "Sukses",
                                    text: data.msg,
                                    icon: "success",
                                    button: "Ok",
                                }).then(function() {
                                    table.ajax.reload();
                                })
                            } else {
                                swal({
                                    title: "Gagal",
                                    text: data.msg,
                                    icon: "error",
                                    button: "Ok",
                                });
                            }
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            swal({
                                title: "Gagal",
                                text: xhr.status,
                                icon: "error",
                                button: "Ok",
                            });
                        }
                    });
                }
            });
    }
    function edit(id) {
        $('#form-data')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
            url: "<?php echo site_url('pengguna/ajax_edit/') ?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $('[name="id"]').val(data.id);
                $('[name="nama"]').val(data.name);
                $('[name="email"]').val(data.email);
                $('#modal_form').modal('show');
                $('#alert').show();

            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error get data from ajax');
            }
        });
    }

    function simpan() {
        // alert($('input[name="jenis_kelamin"]:checked').val())
        if ($('#nama').val() == '') {
            alert("Nama Harus Diisi");
            $('#nama').focus();
            return false;
        }
        if ($('#email').val() == '') {
            alert("Email Harus Diisi");
            $('#email').focus();
            return false;
        }
        if ($('#id').val() == '') {
            if ($('#password').val() == '') {
                alert("Password Harus Diisi");
                $('#password').focus();
                return false;
            }
        }

        data = $('#form-data').serialize();
        swal({
                title: "Apakah anda yakin?",
                text: "Pastikan data yang diinputkan benar!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((simpan) => {
                if (simpan) {
                    $.ajax({
                        type: 'POST',
                        url: "<?php echo base_url(); ?>pengguna/simpan",
                        data: data,
                        cache: false,
                        dataType: 'json',
                        success: function(data) {
                            console.log(data);
                            if (data.code == 200) {
                                swal({
                                    title: "Sukses",
                                    text: data.msg,
                                    icon: "success",
                                    button: "Ok",
                                }).then(function() {
                                    $('#modal_form').modal('hide');
                                    table.ajax.reload();

                                })
                            } else {
                                swal({
                                    title: "Gagal",
                                    text: data.msg,
                                    icon: "error",
                                    button: "Ok",
                                });
                            }
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            swal({
                                title: "Gagal",
                                text: xhr.status,
                                icon: "error",
                                button: "Ok",
                            });
                        }
                    });
                }
            });

    }

    function batal() {
        $('#modal_form').modal('hide');
    }

    function tambah() {
        $('#form-data')[0].reset(); 
        $('#alert').hide();
        $('#modal_form').modal('show');
    }
</script>