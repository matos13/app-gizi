<?php defined('BASEPATH') or exit('No direct script access allowed');

class Pengukuran_model extends CI_Model
{
    private $_table = "pengukuran";

    public $id;
    public $id_pasien;
    public $tanggal;
    public $bb;
    public $tb;
    public $bbi;
    public $imt;
    public $status_gizi;
    public $perubahan_kg;
    public $perubahan_persen;
    public $keterangan;
    public $user_input;
    public $bb_awal;
    public $id_pengukuran_sebelum;
    public $tindak_lanjut;

    public function insert()
    {
        $post = $this->input->post();

        $tanggal = $post["tgl_ukur"];
        $tgl = explode('/', $tanggal);
        $tgl = $tgl[2] . '-' . $tgl[1] . '-' . $tgl[0];

        $this->id_pasien = $post["id_pasien"];
        $this->tanggal = $tgl;
        $this->bb = $post["bb"];
        $this->tb = $post["tb"];
        $this->bbi = $post["bbi"];
        $this->imt = $post["imt"];
        $this->status_gizi = $post["status"];
        $this->perubahan_kg = $post["bb_kg"];
        $this->perubahan_persen = $post["bb_persen"];
        $this->keterangan = $post["keterangan"];
        $this->bb_awal = $post["bb_awal"];
        $this->id_pengukuran_sebelum = $post["id_awal"];
        $this->tindak_lanjut = $post["tindak_lanjut"];
        $this->user_input = $_SESSION['id'];

        return $this->db->insert($this->_table, $this);
    }

    public function update()
    {
        $post = $this->input->post();
        $tanggal = $post["tgl_ukur"];
        $tgl = explode('/', $tanggal);
        $tgl = $tgl[2] . '-' . $tgl[1] . '-' . $tgl[0];
        $this->id_pasien = $post["id_pasien"];
        $this->tanggal = $tgl;
        $this->bb = $post["bb"];
        $this->tb = $post["tb"];
        $this->bbi = $post["bbi"];
        $this->imt = $post["imt"];
        $this->status_gizi = $post["status"];
        $this->perubahan_kg = $post["bb_kg"];
        $this->perubahan_persen = $post["bb_persen"];
        $this->keterangan = $post["keterangan"];
        $this->bb_awal = $post["bb_awal"];
        $this->id_pengukuran_sebelum = $post["id_awal"];
        $this->tindak_lanjut = $post["tindak_lanjut"];
        $this->id = $post["id"];
        $this->user_input = $_SESSION['id'];
        //cek data setelah
        $sebelum = $this->db->query("SELECT * from pengukuran where id_pengukuran_sebelum = $post[id] ")->result();
        if (!empty($sebelum)) {
            if ($post["bb_awal"] != $sebelum[0]->bb_awal) {

                $persen = round(abs((floatval($post["bb"]) - floatval($sebelum[0]->bb)) / floatval($post["bb"]) * 100), 2);
                $kg = round(abs((floatval($post["bb"]) - floatval($sebelum[0]->bb))), 2);
                $ket = '';
                if (floatval($post["bb"]) == floatval($sebelum[0]->bb)) {
                    $ket = 'Tetap';
                } else if (floatval($post["bb"]) < floatval($sebelum[0]->bb)) {
                    $ket = 'Naik';
                } else {
                    $ket = 'Turun';
                }
                $data = array(
                    'perubahan_kg' => $kg,
                    'perubahan_persen' => $persen,
                    'bb_awal' => $post["bb"],
                    'keterangan' => $ket,
                );
                $this->db->where('id', $sebelum[0]->id);
                $this->db->update('pengukuran', $data);
            }
        }

        return $this->db->update($this->_table, $this, array('id' => $post['id']));
    }
    public function data($id)
    {
        return $this->db->query("SELECT * from pengukuran where id_pasien = $id")->result();
    }
    public function ukrterakhir($id)
    {
        return $this->db->query("SELECT id,bb from pengukuran where id_pasien = $id  order by id desc")->result();
    }
    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["id" => $id])->row();
    }
    public function delete($id)
    {
        return $this->db->delete($this->_table, array("id" => $id));
    }
    public function laporan($bulan)
    {
        $dt = explode('-', $bulan);
        return $this->db->query("SELECT pasien.norm,pasien.nama,pengukuran.* from pasien
        join pengukuran on pengukuran.id_pasien = pasien.id
        where year(pengukuran.tanggal)='$dt[1]' and month(pengukuran.tanggal)='$dt[0]' and pasien.flag_hapus='N' order by pengukuran.tanggal asc")->result();
    }
}
