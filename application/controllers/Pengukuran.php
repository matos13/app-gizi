<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pengukuran extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("pengukuran_model");
        $this->load->library('form_validation');
        $this->load->model("pasien_model");
        is_logged_in();
    }

    public function data($id)
    {
        $data['title'] = "Pengukuran";
        $data['konten'] = "Pengukuran";
        $data['pasien'] = $this->pasien_model->getById($id);
        $data['pengukuran'] = $this->pengukuran_model->data($id);
        $data['bbawal'] = $this->pengukuran_model->ukrterakhir($id);
        $this->load->view('pengukuran/data', $data);
    }
    public function imt($id)
    {
        $data['title'] = "Grafik IMT";
        $data['konten'] = "Grafik IMT";
        $data['pasien'] = $this->pasien_model->getById($id);
        $data['pengukuran'] = $this->pengukuran_model->data($id);
        $this->load->view('pengukuran/grafik', $data);
    }
    public function simpan()
    {
        $pengukuran = $this->pengukuran_model;
        if (!empty($this->input->post('id'))) {
            $simpan = $pengukuran->update();
            if ($simpan == 1) {
                $return = array(
                    'code' => 200,
                    'msg' => 'Data Berhasil Diupdate'
                );
            } else {
                $return = array(
                    'code' => 202,
                    'msg' => 'Data Gagal Diupdate'
                );
            }
        } else {
            $simpan = $pengukuran->insert();
            if ($simpan == 1) {
                $return = array(
                    'code' => 200,
                    'msg' => 'Data Berhasil Disimpan'
                );
            } else {
                $return = array(
                    'code' => 202,
                    'msg' => 'Data Gagal Disimpan'
                );
            }
        }
        die(json_encode($return));
    }
    public function ajax_edit($id)
    {
        $data = $this->pengukuran_model->getById($id);
        $data->tanggal = date("d/m/Y", strtotime($data->tanggal));
        echo json_encode($data);
    }
    public function hapus($id)
    {
        $pengukuran = $this->pengukuran_model;
        $simpan = $pengukuran->delete($id);
        if ($simpan == 1) {
            $return = array(
                'code' => 200,
                'msg' => 'Data Berhasil Dihapus'
            );
        } else {
            $return = array(
                'code' => 202,
                'msg' => 'Data Gagal Dihapus'
            );
        }

        die(json_encode($return));
    }
}
