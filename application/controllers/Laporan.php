<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Laporan extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("pengukuran_model");
        $this->load->library('form_validation');
        $this->load->model("pasien_model");
        is_logged_in();
    }

    public function index()
    {
        // print_r($_GET);
        $bulan = isset($_GET['bulan']) ? $_GET['bulan'] : date('m-Y');

        $data['title'] = "Pengukuran";
        $data['konten'] = "Pengukuran";
        $data['laporan'] = $this->pengukuran_model->laporan($bulan);
        $this->load->view('pengukuran/laporan', $data);
    }
    public function export()
    {
        $bulan = $_GET['bulan'];
        $data = $this->pengukuran_model->laporan($bulan);

        include_once APPPATH . '/third_party/xlsxwriter.class.php';
        ini_set('display_errors', 0);
        ini_set('log_errors', 1);
        error_reporting(E_ALL & ~E_NOTICE);

        $filename = "Laporan-" . $bulan . ".xlsx";
        header('Content-disposition: attachment; filename="' . XLSXWriter::sanitize_filename($filename) . '"');
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');

        $styles = array('font' => 'Arial', 'font-size' => 10, 'font-style' => 'bold', 'fill' => '#eee', 'halign' => 'center', 'border' => 'left,right,top,bottom');
        $styles2 = array('font' => 'Arial', 'font-size' => 10, 'font-style' => 'bold', 'halign' => 'center', 'border' => 'left,right,top,bottom');

        $header = array(
            'No' => 'integer',
            'Tanggal Pengukuran' => 'string',
            'No.Rm	' => 'string',
            'Nama' => 'string',
            'BB' => 'string',
            'TB' => 'string',
            'IMT' => 'string',
            'Kurang BB Tingkat Berat' => 'string',
            'Kurang BB Tingkat Ringan' => 'string',
            'Normal' => 'string',
            'Lebih BB Tingkat Ringan' => 'string',
            'Lebih BB Tingkat Berat' => 'string',
            'Naik' => 'string',
            'Turun' => 'string',
            'Tetap' => 'string',
            'Rencana Tindak Lanjut' => 'string',
        );

        $writer = new XLSXWriter();
        $writer->setAuthor('Montazi');

        $writer->writeSheetHeader('Sheet1', $header, $styles);
        $no = 1;
        $kurang1 = 0;
        $kurang2 = 0;
        $lebih1 = 0;
        $lebih2 = 0;
        $normal = 0;
        $turun = 0;
        $naik = 0;
        $tetap = 0;
        foreach ($data as $row) {
            if ($row->status_gizi == 'Kurang BB Tingkat Berat') {
                $kurang2++;
            } elseif ($row->status_gizi == 'Kurang BB Tingkat Ringan') {
                $kurang1++;
            } elseif ($row->status_gizi == 'Normal') {
                $normal++;
            } elseif ($row->status_gizi == 'Lebih BB Tingkat Ringan') {
                $lebih1++;
            } elseif ($row->status_gizi == 'Lebih BB Tingkat Berat') {
                $lebih2++;
            }
            if ($row->keterangan == 'Naik') {
                $naik++;
            } elseif ($row->keterangan == 'Turun') {
                $turun++;
            } elseif ($row->keterangan == 'Tetap') {
                $tetap++;
            }
            $writer->writeSheetRow(
                'Sheet1',
                [
                    $no,
                    tgl_indo($row->tanggal),
                    $row->norm,
                    $row->nama,
                    $row->bb,
                    $row->tb,
                    $row->imt,
                    $row->status_gizi == 'Kurang BB Tingkat Berat' ? 'V' : '',
                    $row->status_gizi == 'Kurang BB Tingkat Ringan' ? 'V' : '',
                    $row->status_gizi == 'Normal' ? 'V' : '',
                    $row->status_gizi == 'Lebih BB Tingkat Ringan' ? 'V' : '',
                    $row->status_gizi == 'Lebih BB Tingkat Berat' ? 'V' : '',
                    $row->keterangan == 'Naik' ? 'V' : '',
                    $row->keterangan == 'Turun' ? 'V' : '',
                    $row->keterangan == 'Tetap' ? 'V' : '',
                    $row->tindak_lanjut,
                ],
                $styles2
            );
            $no++;
        }
        $writer->writeSheetRow(
            'Sheet1',
            [
                '',
                '',
                '',
                '',
                '',
                '',
                'Jumlah',
                $kurang2,
                $kurang1,
                $normal,
                $lebih1,
                $lebih2,
                $naik,
                $turun,
                $tetap
            ],
            $styles2
        );
        $writer->writeToStdOut();
    }
}
