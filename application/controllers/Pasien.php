<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pasien extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model("pasien_model");
		$this->load->library('form_validation');
		is_logged_in();
	}

	public function index()
	{
		$data['title'] = "Pasien";
		$data['konten'] = "Pasien";
		$this->load->view('pasien/data', $data);
	}
	public function tambah()
	{
		$data['title'] = "Pasien";
		$data['konten'] = "Pasien";
		$this->load->view('pasien/tambah', $data);
	}
	public function simpan()
	{
		$pasien = $this->pasien_model;
		$validation = $this->form_validation;
		$validation->set_rules($pasien->rules());
		// if ($validation->run()) {
		// print_r($this->input->post('norm'));
		// exit;
		$cek = $pasien->getByNorm($this->input->post('norm'));

		if (!empty($cek)) {
			$return = array(
				'code' => 203,
				'msg' => 'Data No.RM Double'
			);
		} else {
			$simpan = $pasien->save();
			if ($simpan == 1) {
				$return = array(
					'code' => 200,
					'msg' => 'Data Berhasil Disimpan'
				);
			} else {
				$return = array(
					'code' => 202,
					'msg' => 'Data Gagal Disimpan'
				);
			}
		}
		// }

		die(json_encode($return));
	}
	public function hapus()
	{
		$pasien = $this->pasien_model;
		$simpan = $pasien->hapus();
		if ($simpan == 1) {
			$return = array(
				'code' => 200,
				'msg' => 'Data Berhasil Dihapus'
			);
		} else {
			$return = array(
				'code' => 202,
				'msg' => 'Data Gagal Dihapus'
			);
		}

		die(json_encode($return));
	}

	public function ajax_list()
	{
		header('Content-Type: application/json');
		$list = $this->pasien_model->get_datatables();
		$data = array();
		$no = $this->input->post('start');
		//looping data mahasiswa
		$no = 1;
		foreach ($list as $dt) {
			$row = array();
			//row pertama akan kita gunakan untuk btn edit dan delete
			$lahir    	= new DateTime($dt->tgl_lahir);
			$today    	= new DateTime();
			$umur 	 	= $today->diff($lahir);
			$umur 		= $umur->y . " Tahun " . $umur->m . " Bulan " . $umur->d . " Hari";
			//
			$row[] = $no;
			$row[] = $dt->norm;
			$row[] = $dt->nama;
			$row[] = ($dt->jenis_kelamin == 'L' ? 'Laki-laki' : 'Perempuan');
			$row[] = $umur;
			$row[] = $dt->domisili;
			$row[] =  '<a class="btn btn-success btn-sm" onclick="edit(' . $dt->id . ')"><i class="fa fa-edit"></i> </a>
            <a class="btn btn-danger btn-sm" onclick="hapus(' . $dt->id . ')"><i class="fa fa-trash"></i> </a> 
			<a class="btn btn-warning btn-sm" onclick="monitoring(' . $dt->id . ')"><i class="fa fa-chart-line"></i> </a>
			<a class="btn btn-info btn-sm" onclick="grafik(' . $dt->id . ')"><i class="fa fa-chart-area"></i> </a>';

			$data[] = $row;
			$no++;
		}
		$output = array(
			"draw" => $this->input->post('draw'),
			"recordsTotal" => $this->pasien_model->count_all(),
			"recordsFiltered" => $this->pasien_model->count_filtered(),
			"data" => $data,
		);
		//output to json format
		$this->output->set_output(json_encode($output));
	}
	public function ajax_edit($id)
	{
		$data = $this->pasien_model->getById($id);
		// print_r($data->tgl_lahir);
		$data->tgl_lahir = date("d/m/Y", strtotime($data->tgl_lahir));
		echo json_encode($data);
	}

	public function simpan_edit()
	{
		$pasien = $this->pasien_model;

		$simpan = $pasien->update();
		if ($simpan == 1) {
			$return = array(
				'code' => 200,
				'msg' => 'Data Berhasil Diupdate'
			);
		} else {
			$return = array(
				'code' => 202,
				'msg' => 'Data Gagal Diupdate'
			);
		}

		die(json_encode($return));
	}
}
