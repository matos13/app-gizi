<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pengguna extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("pengguna_model");

        is_logged_in();
    }
    public function index()
    {
        $data['title'] = "Data User";
        $data['konten'] = "User";
        $this->load->view('user/data', $data);
    }
    public function simpan()
    {
        if (empty($this->input->post('id'))) {
            $data = [
                'name' => htmlspecialchars($this->input->post('nama', true)),
                'email' => htmlspecialchars($this->input->post('email', true)),
                'password' => password_hash($this->input->post("password"), PASSWORD_DEFAULT),
                'role_id' => 2

            ];
            $simpan = $this->db->insert('users', $data);
            if ($simpan == 1) {
                $return = array(
                    'code' => 200,
                    'msg' => 'Data Berhasil Disimpan'
                );
            } else {
                $return = array(
                    'code' => 202,
                    'msg' => 'Data Gagal Disimpan'
                );
            }
        } else {
            if (empty($this->input->post("password"))) {
                $data = [
                    'name' => htmlspecialchars($this->input->post('nama', true)),
                    'email' => htmlspecialchars($this->input->post('email', true)),
                    'role_id' => 2

                ];
            } else {
                $data = [
                    'name' => htmlspecialchars($this->input->post('nama', true)),
                    'email' => htmlspecialchars($this->input->post('email', true)),
                    'password' => password_hash($this->input->post("password"), PASSWORD_DEFAULT),
                    'role_id' => 2

                ];
            }
            $simpan = $this->db->update('users', $data, array('id' => $this->input->post('id')));
            if ($simpan == 1) {
                $return = array(
                    'code' => 200,
                    'msg' => 'Data Berhasil Disimpan'
                );
            } else {
                $return = array(
                    'code' => 202,
                    'msg' => 'Data Gagal Disimpan'
                );
            }
        }
        die(json_encode($return));
    }
    public function ajax_list()
    {
        header('Content-Type: application/json');
        $list = $this->pengguna_model->get_datatables();
        $data = array();
        $no = $this->input->post('start');
        //looping data mahasiswa
        $no = 1;
        foreach ($list as $dt) {
            $row = array();
            //row pertama akan kita gunakan untuk btn edit dan delete
            //
            $row[] = $no;
            $row[] = $dt->name;
            $row[] = $dt->email;
            // $row[] = $dt->role_id;
            if ($dt->role_id == 1) {
                $row[] =  '';
            } else {
                $row[] =  '
                <a class="btn btn-success btn-sm" onclick="edit(' . $dt->id . ')"><i class="fa fa-edit"></i> </a>
                <a class="btn btn-danger btn-sm" onclick="hapus(' . $dt->id . ')"><i class="fa fa-trash"></i> </a> 
                <a class="btn btn-warning btn-sm" onclick="reset(' . $dt->id . ')"><i class="fa fa-undo"></i> </a>';
            }


            $data[] = $row;
            $no++;
        }
        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->pengguna_model->count_all(),
            "recordsFiltered" => $this->pengguna_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        $this->output->set_output(json_encode($output));
    }

    public function hapus()
    {
        $pengguna = $this->pengguna_model;
        $simpan = $pengguna->hapus();
        if ($simpan == 1) {
            $return = array(
                'code' => 200,
                'msg' => 'Data Berhasil Dihapus'
            );
        } else {
            $return = array(
                'code' => 202,
                'msg' => 'Data Gagal Dihapus'
            );
        }

        die(json_encode($return));
    }
    public function reset()
    {
        $data = [
            'password' => password_hash('123', PASSWORD_DEFAULT),
        ];
        $simpan = $this->db->update('users', $data, array('id' => $this->input->post('id')));
        if ($simpan == 1) {
            $return = array(
                'code' => 200,
                'msg' => 'Password berhasil direset'
            );
        } else {
            $return = array(
                'code' => 202,
                'msg' => 'Password gagal direset'
            );
        }
        die(json_encode($return));
    }
    public function ajax_edit($id)
    {
        $data = $this->pengguna_model->getById($id);
        echo json_encode($data);
    }
}
